package ppns.ac.portal.akademik.app;

import android.app.Application;
import android.text.TextUtils;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;


/*
    Application controller for communication with web service
    Web Service request build using 3rd party library called Volley
 */
public class GlobalController extends Application {

    public static final String TAG = GlobalController.class.getSimpleName();
    private RequestQueue mRequestQueue;

    private static GlobalController mInstance;


    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static synchronized GlobalController getInstance() {
        return mInstance;
    }

    public RequestQueue getmRequestQueue() {

        if (mRequestQueue == null ) {

            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    //set text if empty
    public <T> void addToReqQueue (Request<T> request, String tag) {

        request.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getmRequestQueue().add(request);

    }

    public <T> void addToReqQueue (Request<T> request) {

        request.setTag(TAG);
        getmRequestQueue().add(request);

    }

    public void cancelPendingReq(Object tag) {

        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
