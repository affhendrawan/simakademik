package ppns.ac.portal.akademik;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.akademik.pens.simakademik.R;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ppns.ac.portal.akademik.app.GlobalController;
import ppns.ac.portal.akademik.model.IpkModel;
import ppns.ac.portal.akademik.adapter.IpkAdapter;
import ppns.ac.portal.akademik.util.AppConfig;
import ppns.ac.portal.akademik.util.SessionManager;


public class NilaiDashboardFragment extends Fragment {

    private static final String TAG = MainActivity.class.getSimpleName();
    //private static final String baseURL = "http://192.168.1.100/slimservice/v1/ipk/";
    //private String paramURL = "6911040033";
    private String url;
    private List<IpkModel> ipkModelList = new ArrayList<IpkModel>();
    private IpkAdapter IpkAdapter;
    private ArrayAdapter<String> smtAdapter;
    private ListView ipkView;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;

    public NilaiDashboardFragment() {
    }






    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_nilai_dashboard, container, false);

        sessionManager = new SessionManager(getActivity().getApplicationContext());
        String stuNumb = sessionManager.getStudentNumber();
        url = AppConfig.URL_IPK+stuNumb;
        Log.i(TAG, url);


        String[] smtList = {"Semester 1", "Semester 2", "Semester 3", "Semester 4", "Semester 5", "Semester 6", "Semester 7", "Semester 8"};
        List<String> smtData = new ArrayList<String>(Arrays.asList(smtList));
        smtAdapter = new ArrayAdapter<String>(getActivity(), R.layout.smt_listview, R.id.smtList, smtData);
        ListView listView = (ListView)rootView.findViewById(R.id.smt_listView);
        listView.setAdapter(smtAdapter);

        listView.setOnItemClickListener(new ListClickHandler());

        IpkAdapter = new IpkAdapter(getActivity(), ipkModelList);
        ipkView = (ListView)rootView.findViewById(R.id.ipkScore);
        ipkView.setAdapter(IpkAdapter);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading . . .");
        progressDialog.show();

        //Send Request
        JsonArrayRequest ipkReq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        hidePDialog();

                        // Parsing json
                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject obj = response.getJSONObject(i);
                                IpkModel ipk = new IpkModel();
                                ipk.setIpk(((Number) obj.getDouble("IPK"))
                                        .doubleValue());

                                ipkModelList.add(ipk);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        IpkAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error: " + error.getMessage());
                hidePDialog();

            }
        });

        GlobalController.getInstance().addToReqQueue(ipkReq);
        return rootView;

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }


    private void hidePDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public class ListClickHandler implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            TextView smtItem = (TextView)view.findViewById(R.id.smtList);
            String smtText = smtItem.getText().toString();
            Intent intent = new Intent(getActivity().getApplication(), NilaiSemester.class);
            intent.putExtra("selected-smt", smtText);
            startActivity(intent);


        }
    }




}
