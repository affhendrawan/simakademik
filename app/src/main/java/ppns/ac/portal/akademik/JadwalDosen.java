package ppns.ac.portal.akademik;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;

import com.akademik.pens.simakademik.R;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ppns.ac.portal.akademik.adapter.JadwalAdapter;
import ppns.ac.portal.akademik.app.GlobalController;
import ppns.ac.portal.akademik.model.JadwalModel;
import ppns.ac.portal.akademik.util.AppConfig;


public class JadwalDosen extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jadwal_dosen);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        private static final String TAG = "jadwal" ;
        private String url;
        private ListView listViewJadwal;
        private EditText cariDosen;
        private List<JadwalModel> jadwalModelList = new ArrayList<JadwalModel>();
        private ProgressDialog progressDialog;
        private JadwalAdapter jadwalAdapter;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_jadwal_dosen, container, false);

            url = AppConfig.URL_JADWAL;
            GlobalController.getInstance().addToReqQueue(handleRequest(url));

            listViewJadwal = (ListView)rootView.findViewById(R.id.jadwal_list);
            cariDosen = (EditText)rootView.findViewById(R.id.cari_dosen);
            jadwalAdapter = new JadwalAdapter(getActivity(), jadwalModelList);
            listViewJadwal.setAdapter(jadwalAdapter);

            registerForContextMenu(listViewJadwal);
            listViewJadwal.setTextFilterEnabled(true);

            cariDosen.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (count < before) {
                        jadwalAdapter.resetData();
                    }
                    jadwalAdapter.getFilter().filter(s);
                    Log.d("Check", s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading . . .");
            progressDialog.show();

            return rootView;
        }


        private JsonArrayRequest handleRequest(String url){
            JsonArrayRequest jadwalReq = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d(TAG, response.toString());
                            hidePDialog();
                            // Parsing json
                            for (int i = 0; i < response.length(); i++) {
                                try {

                                    JSONObject obj = response.getJSONObject(i);
                                    JadwalModel jadwalModel = new JadwalModel();
                                    jadwalModel.setNamaDosen(obj.getString("lecturer_name"));
                                    jadwalModel.setHari(obj.getInt("hour_day"));
                                    jadwalModel.setRuang(obj.getString("room_name"));
                                    jadwalModel.setStartTime(obj.getString("from"));
                                    jadwalModel.setEndTime(obj.getString("to"));

                                    jadwalModelList.add(jadwalModel);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                            // notifying list adapter about data changes
                            // so that it renders the list view with updated data
                            jadwalAdapter.notifyDataSetChanged();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error: " + error.getMessage());
                }
            });

            return jadwalReq;
        }


        @Override
        public void onDestroy() {
            super.onDestroy();
            hidePDialog();
        }

        /**
         * Hide Progress Dialog
         */
        private void hidePDialog(){
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }
}
