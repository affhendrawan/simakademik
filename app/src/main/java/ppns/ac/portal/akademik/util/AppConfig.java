package ppns.ac.portal.akademik.util;

/**
 * Class For Define API URL
 */
public class AppConfig {

    private static String URL_BASE = "http://10.216.158.83/slimservice/v1/";
    public static String URL_LOGIN = URL_BASE+"login";
    public static String URL_IPK = URL_BASE+"ipk/";
    public static String URL_SCORESMT = URL_BASE+"scoresmt/";
    public static String URL_IPS = URL_BASE+"ips/";
    public static String URL_JADWAL = URL_BASE+"jadwaldosen";

}
