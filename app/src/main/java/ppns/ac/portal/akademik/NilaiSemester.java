package ppns.ac.portal.akademik;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import com.akademik.pens.simakademik.R;

import ppns.ac.portal.akademik.adapter.IpsAdapter;
import ppns.ac.portal.akademik.adapter.NilaiAdapter;
import ppns.ac.portal.akademik.app.GlobalController;
import ppns.ac.portal.akademik.model.IpsModel;
import ppns.ac.portal.akademik.model.NilaiModel;
import ppns.ac.portal.akademik.util.AppConfig;
import ppns.ac.portal.akademik.util.SessionManager;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class NilaiSemester extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nilai_semester);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_penilaian, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        private static final String TAG = MainActivity.class.getSimpleName();
        //private static final String baseURL = "http://192.168.1.100/slimservice/v1/scoresmt/";
        //private static final String baseURLIps = "http://192.168.1.100/slimservice/v1/ips/";
        //private String paramURL;
        private String url, url2;
        private List<NilaiModel> nilaiModelList = new ArrayList<NilaiModel>();
        private List<IpsModel> ipsModelList = new ArrayList<IpsModel>();
        private GridView gridView;
        private ListView ipsView;
        private NilaiAdapter adapter;
        private IpsAdapter ipsAdapter;

        private ProgressDialog progressDialog;
        private SessionManager sessionManager;


        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_nilai_semester, container, false);

            sessionManager = new SessionManager(getActivity().getApplicationContext());

            Intent intent = getActivity().getIntent();
            String getSmt = intent.getStringExtra("selected-smt");
            String stuNumb = sessionManager.getStudentNumber();
            switch(getSmt){
                case "Semester 1":
                    url = AppConfig.URL_SCORESMT+stuNumb+"/1";
                    url2 = AppConfig.URL_IPS+stuNumb+"/1";
                    Log.i(TAG, url);
                    break;
                case "Semester 2":
                    url = AppConfig.URL_SCORESMT+stuNumb+"/2";
                    url2 = AppConfig.URL_IPS+stuNumb+"/2";
                    break;
                case "Semester 3":
                    url = AppConfig.URL_SCORESMT+stuNumb+"/3";
                    url2 = AppConfig.URL_IPS+stuNumb+"/3";
                    break;
                case "Semester 4":
                    url = AppConfig.URL_SCORESMT+stuNumb+"/4";
                    url2 = AppConfig.URL_IPS+stuNumb+"/4";
                    break;
                case "Semester 5":
                    url = AppConfig.URL_SCORESMT+stuNumb+"/5";
                    url2 = AppConfig.URL_IPS+stuNumb+"/5";
                    break;
                case "Semester 6":
                    url = AppConfig.URL_SCORESMT+stuNumb+"/6";
                    url2 = AppConfig.URL_IPS+stuNumb+"/6";
                    break;
                case "Semester 7":
                    url = AppConfig.URL_SCORESMT+stuNumb+"/7";
                    url2 = AppConfig.URL_IPS+stuNumb+"/7";
                    break;
                case "Semester 8":
                    url = AppConfig.URL_SCORESMT+stuNumb+"/8";
                    url2 = AppConfig.URL_IPS+stuNumb+"/8";
                    break;
            }



            gridView = (GridView)rootView.findViewById(R.id.gridViewNilai);
            adapter = new NilaiAdapter(getActivity(), nilaiModelList);
            gridView.setAdapter(adapter);

            ipsView = (ListView)rootView.findViewById(R.id.ipsScore);
            ipsAdapter = new IpsAdapter(getActivity(), ipsModelList);
            ipsView.setAdapter(ipsAdapter);

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading . . .");
            progressDialog.show();

            JsonArrayRequest nilaiReq = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d(TAG, response.toString());
                            hidePDialog();

                            // Parsing json
                            for (int i = 0; i < response.length(); i++) {
                                try {

                                    JSONObject obj = response.getJSONObject(i);
                                    NilaiModel nilai = new NilaiModel();
                                    nilai.setMatKul(obj.getString("curriculum_subjectname"));
                                    nilai.setNilai(obj.getString("score_scoreconversion"));
                                    nilai.setSks(obj.getInt("curriculum_credit"));

                                    nilaiModelList.add(nilai);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                            // notifying list adapter about data changes
                            // so that it renders the list view with updated data
                            adapter.notifyDataSetChanged();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error: " + error.getMessage());
                    hidePDialog();

                }
            });


            JsonArrayRequest ipsReq = new JsonArrayRequest(url2,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d(TAG, response.toString());
                            hidePDialog();

                            // Parsing json
                            for (int i = 0; i < response.length(); i++) {
                                try {

                                    JSONObject obj = response.getJSONObject(i);
                                    IpsModel ips = new IpsModel();
                                    ips.setIps(((Number) obj.getDouble("IPS"))
                                            .doubleValue());

                                    ipsModelList.add(ips);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                            // notifying list adapter about data changes
                            // so that it renders the list view with updated data
                            ipsAdapter.notifyDataSetChanged();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error: " + error.getMessage());
                    hidePDialog();

                }
            });


            GlobalController.getInstance().addToReqQueue(nilaiReq);
            GlobalController.getInstance().addToReqQueue(ipsReq);

            return rootView;
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            hidePDialog();
        }

        private void hidePDialog() {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }
}
