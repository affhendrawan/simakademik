package ppns.ac.portal.akademik.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.akademik.pens.simakademik.R;

import org.w3c.dom.Text;

import ppns.ac.portal.akademik.model.NilaiModel;

import java.util.List;

/*
    Adapter for NilaiSemester
 */
public class NilaiAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater mInflater;
    private List<NilaiModel> nilaiModelList;

    public NilaiAdapter(Activity activity, List<NilaiModel> nilaiModelList) {

        this.activity = activity;
        this.nilaiModelList = nilaiModelList;

    }


    @Override
    public int getCount() {
        return nilaiModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return nilaiModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {



        if(mInflater == null){
            mInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.custom_grid_nilai, parent, false);

            TextView namaMatkul = (TextView)convertView.findViewById(R.id.namaMatkul);
            TextView nilaiConvert = (TextView)convertView.findViewById(R.id.nilaiConversi);
            TextView sks = (TextView)convertView.findViewById(R.id.sks);

            namaMatkul.setPadding(10, 10, 10, 10);
            nilaiConvert.setPadding(10, 10, 10, 10);
            sks.setPadding(10, 10, 10, 10);

            NilaiModel model = nilaiModelList.get(position);

            namaMatkul.setText(model.getMatKul());
            nilaiConvert.setText(model.getNilai());
            sks.setText(String.valueOf(model.getSks()));

        }

        return convertView;
    }

}
