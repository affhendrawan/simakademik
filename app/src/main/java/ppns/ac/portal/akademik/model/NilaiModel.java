package ppns.ac.portal.akademik.model;

/*
    Model for Nilai Semester
    for handling data from web service
 */
public class NilaiModel {

    private String matKul, nilai;
    private int sks;

    public NilaiModel(){

    }

    public NilaiModel(String matKul, String nalai, int sks) {

        this.matKul = matKul;
        this.nilai = nalai;
        this.sks = sks;

    }

    public String getMatKul() {
        return matKul;
    }

    public void setMatKul(String matKul) {

        this.matKul = matKul;
    }

    public String getNilai() {
        return nilai;
    }

    public void setNilai(String nilai) {
        this.nilai = nilai;
    }

    public int getSks() {
        return sks;
    }

    public void setSks(int sks) {
        this.sks = sks;
    }

}
