package ppns.ac.portal.akademik.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.akademik.pens.simakademik.R;

import java.util.List;

import ppns.ac.portal.akademik.model.IpkModel;

/*
    Adapter for IPK
 */
public class IpkAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater mInflater;
    private List<IpkModel> ipkModelList;

    public IpkAdapter(Activity activity, List<IpkModel> ipkModelList) {

        this.activity = activity;
        this.ipkModelList = ipkModelList;

    }

    @Override
    public int getCount() { return ipkModelList.size();}

    @Override
    public Object getItem(int position) {
        return ipkModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(mInflater == null){
            mInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.custom_text_ipk, parent, false);

            TextView ipk = (TextView)convertView.findViewById(R.id.ipkScore_custom);

            ipk.setPadding(10, 10, 10, 10);

            IpkModel model = ipkModelList.get(position);

            ipk.setText(String.valueOf(model.getIpk()));


        }

        return convertView;
    }

}
