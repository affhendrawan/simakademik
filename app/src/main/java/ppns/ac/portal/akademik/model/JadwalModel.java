package ppns.ac.portal.akademik.model;

/**
 * Created by L on 14/06/2015.
 */
public class JadwalModel {

    private String namaDosen, hari, ruang, startTime, endTime;

    public JadwalModel() {

    }

    public JadwalModel(String namaDosen, String hari, String ruang, String startTime, String endTime){

        this.namaDosen = namaDosen;
        this.hari = hari;
        this.ruang = ruang;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getNamaDosen() {
        return namaDosen;
    }

    public void setNamaDosen(String namaDosen) {
        this.namaDosen = namaDosen;
    }

    public String getHari() { return hari;}

    public void setHari(int hari) {
        switch (hari){
            case 1:
                this.hari = "Senin";
                break;
            case 2:
                this.hari = "Selasa";
                break;
            case 3:
                this.hari = "Rabu";
                break;
            case 4:
                this.hari = "Kamis";
                break;
            case 5:
                this.hari = "Jumat";
                break;
            default:
                break;
        }
    }

    public String getRuang() {
        return ruang;
    }

    public void setRuang(String ruang) {
        this.ruang = ruang;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

}
