package ppns.ac.portal.akademik.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.akademik.pens.simakademik.R;

import java.util.ArrayList;
import java.util.List;

import ppns.ac.portal.akademik.model.JadwalModel;

/**
 * Created by L on 14/06/2015.
 */
public class JadwalAdapter extends ArrayAdapter<JadwalModel> implements Filterable {

    private LayoutInflater layoutInflater;
    private List<JadwalModel> jadwalModelList;
    private List<JadwalModel> jadwalModelListFilter;
    private Context context;
    private ModelFilter modelFilter;



    public JadwalAdapter(Context context,List<JadwalModel> jadwalModelList) {
        super(context, R.layout.custom_list_jadwal, jadwalModelList);

        this.context = context;
        this.jadwalModelListFilter = jadwalModelList;
        this.jadwalModelList = jadwalModelList;
    }

    @Override
    public int getCount() {
        return jadwalModelListFilter.size();
    }

    public JadwalModel getItem(int position){
        return jadwalModelListFilter.get(position);
    }

    public long getItemId(int position){
        return  jadwalModelListFilter.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if(convertView == null) {
            layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.custom_list_jadwal, parent, false);

        }

        TextView namaDosen = (TextView)convertView.findViewById(R.id.nama_dosen);
        TextView hari = (TextView)convertView.findViewById(R.id.hari);
        TextView ruangan = (TextView)convertView.findViewById(R.id.ruangan);
        TextView startTime = (TextView)convertView.findViewById(R.id.start_time);
        TextView endTime = (TextView)convertView.findViewById(R.id.end_time);

        JadwalModel model = jadwalModelListFilter.get(position);
        namaDosen.setText(model.getNamaDosen());
        hari.setText(model.getHari());
        ruangan.setText(model.getRuang());
        startTime.setText(model.getStartTime());
        endTime.setText(model.getEndTime());


        return convertView;
    }

    public void resetData() {
        jadwalModelListFilter = jadwalModelList;
    }

    @Override
    public Filter getFilter() {

        if(modelFilter == null) {
            modelFilter = new ModelFilter();
        }

        return modelFilter;
    }

    private static class ViewHolder {
        TextView namaDosen;
        TextView hari;
        TextView ruangan;
        TextView startTime;
        TextView endTime;
    }

    private class ModelFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();

            if(constraint == null || constraint.length() == 0) {
                filterResults.values = jadwalModelList;
                filterResults.count = jadwalModelList.size();
            } else {
                List<JadwalModel> filteredJadwal = new ArrayList<JadwalModel>();

                for (int i = 0, l = jadwalModelList.size(); i < l; i++){
                    JadwalModel model = jadwalModelList.get(i);
                    if(model.getNamaDosen().toLowerCase().contains(constraint.toString().toLowerCase())){
                        filteredJadwal.add(model);
                    }
                }

                filterResults.values = filteredJadwal;
                filterResults.count = filteredJadwal.size();
            }

            return filterResults;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if(results.count == 0) {
                notifyDataSetInvalidated();
            } else {
                jadwalModelListFilter = (List<JadwalModel>)results.values;
                notifyDataSetChanged();
            }
        }
    }
}
