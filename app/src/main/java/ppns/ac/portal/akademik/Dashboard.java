package ppns.ac.portal.akademik;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.akademik.pens.simakademik.R;

import ppns.ac.portal.akademik.util.SessionManager;


public class Dashboard extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.actionbar);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        private static final String TAG = Dashboard.class.getSimpleName();
        private SessionManager sessionManager;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);

            //Intent intent = getActivity().getIntent();

            Button jadwalBtn = (Button)rootView.findViewById(R.id.jadwalDosen);
            Button nilaiBtn = (Button)rootView.findViewById(R.id.penilaian);
            Button aboutBtn = (Button)rootView.findViewById(R.id.about);
            Button logoutBtn = (Button)rootView.findViewById(R.id.logout);


            jadwalBtn.setOnClickListener(new View.OnClickListener(){

                public void onClick(View v) {

                    Intent i = new Intent(getActivity().getApplicationContext(), JadwalDosen.class);
                    startActivity(i);

                }
            });

            nilaiBtn.setOnClickListener(new View.OnClickListener(){

                public void onClick(View v) {

                    Intent i = new Intent(getActivity().getApplicationContext(), NilaiDashboard.class);
                    //i.putExtra("student_number", stuNumb);
                    startActivity(i);

                }
            });

            aboutBtn.setOnClickListener(new View.OnClickListener(){

                public void onClick(View v) {

                    Intent i = new Intent(getActivity().getApplicationContext(), About.class);
                    startActivity(i);

                }
            });

            logoutBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().finish();
                }
            });


            return rootView;
        }

        public void AppExit(){
            sessionManager.setLogin(false);
            this.getActivity().finish();
        }
    }
}
