package ppns.ac.portal.akademik;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



import com.akademik.pens.simakademik.R;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ppns.ac.portal.akademik.app.GlobalController;
import ppns.ac.portal.akademik.util.AppConfig;
import ppns.ac.portal.akademik.util.SessionManager;

public class LoginFragment extends Fragment {

    private static final String TAG = LoginFragment.class.getSimpleName();
    //private static final String url = "http://192.168.1.100/slimservice/v1/login";
    private int loginFailNumber = 3;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;

    public LoginFragment() {
    }


    public void onCreate(Bundle savedInstanseState) {
        super.onCreate(savedInstanseState);
        //set option to fragment
        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);



        //Set Variable
        final Button loginBtn = (Button)rootView.findViewById(R.id.loginBtn);
        final EditText uname = (EditText)rootView.findViewById(R.id.unameText);
        final EditText pass = (EditText)rootView.findViewById(R.id.passText);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);


        sessionManager = new SessionManager(getActivity().getApplicationContext());

        /*
        if(sessionManager.isLoggedIn()) {
            Intent intent = new Intent(getActivity().getApplicationContext(), Dashboard.class);
            startActivity(intent);
        }
        */




        loginBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String studentNumb = uname.getText().toString();
                String password = pass.getText().toString();

                if (studentNumb.trim().length() > 0 && password.trim().length() > 0) {
                    checkLogin(studentNumb, password);
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Please Input Username and Password !", Toast.LENGTH_LONG).show();
                }

            }
        });



        return rootView;
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();
    }

    private void checkLogin (final String student_number, final String pass) {

        String tag_string_req = "req_login";

        progressDialog.setMessage("Logging in . . .");
        showDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.URL_LOGIN, new Response.Listener<String>() {
            @Override
                public void onResponse(String response) {
                    Log.d(TAG, "Login Response : " + response.toString());
                    hideDialog();

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean error = jsonObject.getBoolean("error");
                        String stuName = jsonObject.getString("student_name");
                        String stuNumb = jsonObject.getString("student_number");

                        if (!error) {
                            sessionManager.setLogin(true);
                            sessionManager.setStudentNumber(stuNumb);

                            Intent intent = new Intent(getActivity().getApplication(), Dashboard.class);
                            Toast.makeText(getActivity().getApplicationContext(), "Welcome " + stuName +" " + stuNumb, Toast.LENGTH_SHORT).show();
                            intent.putExtra("student_number", stuNumb);
                            startActivity(intent);
                        } else  {
                            String errorMsg = jsonObject.getString("message");
                            Toast.makeText(getActivity().getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        Toast.makeText(getActivity().getApplicationContext(), "Login Failed!\nCheck Your Username or Password", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error: " + error.getMessage());
                    hideDialog();
                }

        }){
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("student_id", student_number);
                params.put("pass", pass);

                return params;
            }
        };

        GlobalController.getInstance().addToReqQueue(stringRequest, tag_string_req);

    }

    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

}