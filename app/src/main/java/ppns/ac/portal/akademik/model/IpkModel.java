package ppns.ac.portal.akademik.model;

/*
    Model for IPK
    for handling data from web service
 */
public class IpkModel {

    private double ipk;

    public IpkModel(){
    }

    public IpkModel(double ipk) {
        this.ipk = ipk;
    }

    public double getIpk() {
        return ipk;
    }

    public void setIpk(double ipk) {

        this.ipk = ipk;
    }

}
