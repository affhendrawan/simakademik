package ppns.ac.portal.akademik.adapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.akademik.pens.simakademik.R;

import java.util.List;

import ppns.ac.portal.akademik.model.IpsModel;

public class IpsAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater mInflater;
    private List<IpsModel> ipsModelList;

    public IpsAdapter(Activity activity, List<IpsModel> ipsModelList) {

        this.activity = activity;
        this.ipsModelList = ipsModelList;

    }

    @Override
    public int getCount() { return ipsModelList.size();}

    @Override
    public Object getItem(int position) {
        return ipsModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(mInflater == null){
            mInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.custom_text_ips, parent, false);

            TextView ips = (TextView)convertView.findViewById(R.id.ipsScore_custom);

            ips.setPadding(10, 10, 10, 10);

            IpsModel model = ipsModelList.get(position);

            ips.setText(String.valueOf(model.getIps()));


        }

        return convertView;
    }

}
